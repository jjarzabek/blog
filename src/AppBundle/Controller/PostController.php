<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Post controller.
 *
 */
class PostController extends Controller
{
    /**
     * Lists all Post entities.
     *
     * @Route("/", name="post_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('AppBundle:Post')->findAll();

        return $this->render('AppBundle:Post:index.html.twig', array(
            'posts' => $posts
        ));
    }

    /**
     * Creates a new Post entity.
     *
     * @Route("post/new", name="post_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm('AppBundle\Form\PostType', $post, array(
            'show_legend' => false
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $post->setSlug($this->get('cocur_slugify')->slugify($post->getTitle()));
            $em->persist($post);

            try {
                $em->flush();
                return $this->redirectToRoute('post_show', array('slug' => $post->getSlug()));
            }
            catch (UniqueConstraintViolationException $e){
                $form->addError(new FormError('Istnieje już wpis o takim samym slag-u.'));
            }
        }

        return $this->render('AppBundle:Post:new.html.twig', array(
            'post' => $post,
            'form' => $form->createView()
        ));
    }

    /**
     * Finds and displays a Post entity.
     *
     * @Route("post/{slug}", name="post_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Post $post)
    {
        $comment = new Comment();

        $form = $this->createForm('AppBundle\Form\CommentType', $comment, array(
            'show_legend' => false
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $comment->setPost($post);
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('post_show', array('slug' => $post->getSlug()));
        }

        $deleteForm = $this->createDeleteForm($post);

        return $this->render('AppBundle:Post:show.html.twig', array(
            'post' => $post,
            'delete_form' => $deleteForm->createView(),
            'comment_form' => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("post/{id}/edit", name="post_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Post $post)
    {
        $deleteForm = $this->createDeleteForm($post);
        $editForm = $this->createForm('AppBundle\Form\PostType', $post, array(
            'show_legend' => false
        ));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('post_edit', array('id' => $post->getId()));
        }

        return $this->render('AppBundle:Post:edit.html.twig', array(
            'post' => $post,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("post/{id}", name="post_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Post $post)
    {
        $form = $this->createDeleteForm($post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();
        }

        return $this->redirectToRoute('post_index');
    }

    /**
     * Creates a form to delete a Post entity.
     *
     * @param Post $post The Post entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Post $post)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('post_delete', array('id' => $post->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
